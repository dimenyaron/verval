package service;

import domain.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;

import java.util.ArrayList;
import java.util.Collections;

public class ServiceMockTest {

    @Mock
    StudentXMLRepository studentRepository;
    @Mock
    HomeworkXMLRepository homeworkRepository;
    @Mock
    GradeXMLRepository gradeRepository;

    Service service;

    @Captor
    ArgumentCaptor<Student> argumentCaptor;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        service = new Service(studentRepository, homeworkRepository, gradeRepository);
    }

    @Test
    public void studentArrayShouldBeReturned(){
        Mockito.when(studentRepository.findAll()).thenReturn(new ArrayList<>());
        Iterable<Student> students = service.findAllStudents();
        Assertions.assertNotNull(students);
    }

    @Test
    public void newStudentShouldBeAdded() {

        Mockito.when(studentRepository.save(Mockito.any(Student.class))).thenReturn(null);
        Mockito.when(studentRepository.findAll()).thenReturn(new ArrayList<>());

        Iterable<Student> students = service.findAllStudents();
        int count1 = 0;
        for (Student ignored : students) {
            count1++;
        }

        int returnValue = service.saveStudent("test","name",111);
        Mockito.verify(studentRepository).save(argumentCaptor.capture());

        Assertions.assertEquals("test",argumentCaptor.getValue().getID());
        Assertions.assertEquals("name",argumentCaptor.getValue().getName());
        Assertions.assertEquals(111,argumentCaptor.getValue().getGroup());

        Assertions.assertEquals(1,returnValue);

        Mockito.when(studentRepository.findAll()).thenReturn(new ArrayList<>(Collections.singletonList(new Student("test","name",111))));

        students = service.findAllStudents();

        int found = 0;
        int count2 = 0;
        for (Student addedStudent : students) {
            if(addedStudent.getID().equals("test") && addedStudent.getName().equals("name") && addedStudent.getGroup() == 111){
                found = 1;
            }
            count2++;
        }

        Assertions.assertEquals(1,found);
        Assertions.assertEquals(count1+1,count2);

    }

    @ParameterizedTest
    @ValueSource(strings = {"testId","id1","id3"})
    public void existingStudentShouldBeUpdated(String id){

        Mockito.when(studentRepository.update(Mockito.any(Student.class))).thenReturn(new Student(id,"nameModified",111));

        int returnValue = service.updateStudent(id,"nameModified",111);
        Assertions.assertEquals(1,returnValue);

        Mockito.when(studentRepository.findAll()).thenReturn(new ArrayList<>(Collections.singletonList(new Student(id,"nameModified",111))));

        Iterable<Student> students = service.findAllStudents();
        int found = 0;
        for (Student addedStudent : students) {
            if(addedStudent.getID().equals(id) && addedStudent.getName().equals("nameModified") && addedStudent.getGroup() == 111){
                found = 1;
                break;
            }
        }
        Assertions.assertEquals(1,found);
    }
}
