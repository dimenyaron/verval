package service;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.GradeValidator;
import validation.HomeworkValidator;
import validation.StudentValidator;
import validation.Validator;

public class ServiceTest {

    private static Service service;

    @BeforeAll
    static void initAll() {
        Validator<Student> studentValidator = new StudentValidator();
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @Test
    public void studentArrayShouldBeReturned() {
        Iterable<Student> students = service.findAllStudents();
        Assertions.assertNotNull(students);
    }

    @Test
    public void newStudentShouldBeAdded() {

        Iterable<Student> students = service.findAllStudents();
        int count1 = 0;
        for (Student ignored : students) {
            count1++;
        }

        int returnValue = service.saveStudent("test", "name", 111);
        Assertions.assertEquals(1, returnValue);
        students = service.findAllStudents();
        int found = 0;
        int count2 = 0;
        for (Student addedStudent : students) {
            if (addedStudent.getID().equals("test") && addedStudent.getName().equals("name") && addedStudent.getGroup() == 111) {
                found = 1;
            }
            count2++;
        }

        Assertions.assertEquals(1, found);
        Assertions.assertEquals(count1 + 1, count2);

        service.deleteStudent("test");
    }

    @ParameterizedTest
    @ValueSource(strings = {"testing", "id1test", "id3test"})
    public void existingStudentShouldBeUpdated(String id) {
        service.saveStudent(id, "name", 111);
        int returnValue = service.updateStudent(id, "nameModified", 111);
        Assertions.assertTrue(returnValue == 1);
        Iterable<Student> students = service.findAllStudents();
        int found = 0;
        for (Student addedStudent : students) {
            System.out.println(addedStudent);
            if (addedStudent.getID().equals(id) && addedStudent.getName().equals("nameModified") && addedStudent.getGroup() == 111) {
                found = 1;
                break;
            }
        }
        Assertions.assertEquals(1, found);
        service.deleteStudent(id);
    }

    @Test
    public void newHomeworkShouldBeAdded() {

        Iterable<Homework> homeworks = service.findAllHomework();
        int count1 = 0;
        for (Homework ignored : homeworks) {
            count1++;
        }

        int returnValue = service.saveHomework("testing", "testDescription", 8, 7);
        Assertions.assertEquals(1, returnValue);
        homeworks = service.findAllHomework();
        int found = 0;
        int count2 = 0;
        for (Homework addedHomework : homeworks) {
            if (addedHomework.getID().equals("testing") && addedHomework.getDescription().equals("testDescription") && addedHomework.getDeadline() == 8 && addedHomework.getStartline() == 7) {
                found = 1;
            }
            count2++;
        }

        Assertions.assertEquals(1, found);
        Assertions.assertEquals(count1 + 1, count2);

        service.deleteHomework("testing");
    }

    @Test
    public void existingHomeworkShouldBeUpdated() {
        service.saveHomework("testing", "description", 8,7);
        int returnValue = service.updateHomework("testing", "descriptionModified", 8,7);
        Assertions.assertTrue(returnValue == 1);
        Iterable<Homework> homeworks = service.findAllHomework();
        int found = 0;
        for (Homework addedHomework: homeworks) {
            if (addedHomework.getID().equals("testing") && addedHomework.getDescription().equals("descriptionModified") && addedHomework.getDeadline() == 8 && addedHomework.getStartline() == 7) {
                found = 1;
                break;
            }
        }
        Assertions.assertEquals(1, found);
        service.deleteHomework("testing");
    }
}
